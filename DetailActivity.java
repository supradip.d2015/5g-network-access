package in.android5g.fiveg;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.analytics.FirebaseAnalytics;

public class DetailActivity extends AppCompatActivity {
    InterstitialAd mInterstitialAd;
    private WebView myWebView;

    /* access modifiers changed from: private */
    public void requestNewInterstitial() {
        this.mInterstitialAd.loadAd(new AdRequest.Builder().build());
    }

    public void onBackPressed() {
        if (this.mInterstitialAd.isLoaded()) {
            this.mInterstitialAd.show();
            finish();
            return;
        }
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_detail);
        this.mInterstitialAd = new InterstitialAd(this);
        this.mInterstitialAd.setAdUnitId("ca-app-pub-4297693171865380/6489314558");
        this.mInterstitialAd.setAdListener(new AdListener() {
            public void onAdClosed() {
                DetailActivity.this.requestNewInterstitial();
            }
        });
        requestNewInterstitial();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AdView) findViewById(R.id.adView_detail)).loadAd(new AdRequest.Builder().build());
        Bundle extras = getIntent().getExtras();
        String string = extras.getString(FirebaseAnalytics.Param.VALUE);
        String string2 = extras.getString(FirebaseAnalytics.Param.LEVEL);
        setTitle(string);
        this.myWebView = (WebView) findViewById(R.id.web_View);
        WebSettings settings = this.myWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        WebView webView = this.myWebView;
        webView.loadUrl("file:///android_asset/" + string2 + ".htm");
        settings.setBuiltInZoomControls(true);
        if (Build.VERSION.SDK_INT >= 11) {
            settings.setDisplayZoomControls(false);
        }
        this.myWebView.setWebViewClient(new WebViewClient());
    }
}

