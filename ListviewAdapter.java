package in.android5g.fiveg;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.android5g.textdrawable.TextDrawable;
import com.android5g.textdrawable.util.ColorGenerator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ListViewAdapter extends ArrayAdapter<String> {
    private MainActivity activity;
    private List<String> friendList;
    private List<String> searchList = new ArrayList();

    private class ViewHolder {
        /* access modifiers changed from: private */
        public TextView friendName;
        /* access modifiers changed from: private */
        public ImageView imageView;

        public ViewHolder(View view) {
            this.imageView = (ImageView) view.findViewById(R.id.image_view);
            this.friendName = (TextView) view.findViewById(R.id.text);
        }
    }

    public ListViewAdapter(MainActivity mainActivity, int i, List<String> list) {
        super(mainActivity, i, list);
        this.activity = mainActivity;
        this.friendList = list;
        this.searchList.addAll(this.friendList);
    }

    public void filter(String str) {
        String lowerCase = str.toLowerCase(Locale.getDefault());
        this.friendList.clear();
        if (lowerCase.length() == 0) {
            this.friendList.addAll(this.searchList);
        } else {
            for (String next : this.searchList) {
                if (next.toLowerCase(Locale.getDefault()).contains(lowerCase)) {
                    this.friendList.add(next);
                }
            }
        }
        notifyDataSetChanged();
    }

    public int getCount() {
        return this.friendList.size();
    }

    public String getItem(int i) {
        return this.friendList.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        LayoutInflater layoutInflater = (LayoutInflater) this.activity.getSystemService("layout_inflater");
        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_listview, viewGroup, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.friendName.setText(getItem(i));
        char charAt = getItem(i).charAt(0);
        viewHolder.imageView.setImageDrawable(TextDrawable.builder().buildRound(String.valueOf(charAt), ColorGenerator.MATERIAL.getColor(getItem(i))));
        return view;
    }
}

