package in.android5g.fiveg;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.firebase.analytics.FirebaseAnalytics;

public class InformationActivity extends AppCompatActivity {
    private WebView myWebView;

    public void onBackPressed() {
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_detail_info);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle extras = getIntent().getExtras();
        String string = extras.getString(FirebaseAnalytics.Param.VALUE);
        String string2 = extras.getString(FirebaseAnalytics.Param.LEVEL);
        setTitle(string);
        this.myWebView = (WebView) findViewById(R.id.web_View);
        WebSettings settings = this.myWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        this.myWebView.loadUrl(string2);
        settings.setBuiltInZoomControls(true);
        if (Build.VERSION.SDK_INT >= 11) {
            settings.setDisplayZoomControls(false);
        }
        this.myWebView.setWebViewClient(new WebViewClient());
    }
}
