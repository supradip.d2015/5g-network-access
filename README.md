# 5G network access

Source for 5G network access 
5G - Overview

Wireless connectivity is now an integral part of modern life, and we’re moving towards a new digital era. With 5G on the horizon, 

 

At first, we just had voice calls. Then came SMS and eventually, with GPRS and 3G, mobile internet was born. Even if 4G has made everything move much more quickly, our need for more speed continues to grow; and with our reliance on connected ‘things’ – not just phones – also building rapidly, the industry is firmly focused on the next generation: 5G.

What is 5G?

That question is harder to answer than you might think, as there’s currently no definition around which technologies will identify as 5G – nor are there any common global standards. Although they are being worked on.



From a broader perspective, though, 5G is the next big stage in the evolution of wireless connectivity. The general consensus, supported by the GSMA, is that there will be two major strands: an evolutionary piece aimed at making all existing and future mobile technologies – including WiFi - play nicely together, and the revolutionary piece, which is focused on massive speed and low latency.

The specific criteria may differ from source to source, but they all point to the same types of improvement: greater speeds, better capacity, more reliability, enhanced efficiency and reduced latency. There’s certainly a lot of promise.

How it stacks up

In terms of figures, the GSMA suggests 5G networks will offer bandwidth throughput as high as 1Gbps and beyond, with latency as low as one millisecond. This would enable a host of new services, including autonomous driving, augmented reality and virtual reality.

These thoughts are aligned with the definitions of ITU (International Telecommunications Union), which expects 5G to enable the applications mentioned above as well as enhanced industry automation, cloud services, smart cities and smart buildings. ITU has also defined three main characteristics for 5G: Enhanced mobile broadband; massive machine-type communications; and ultra-reliable and low latency communications.

For some perspective, the highest average 4G speeds in the UK are still less than 30Mbps per second.

The GSMA and ITU’s figures are just baseline expectations, however – trials suggest it could be considerably faster still, with some in the industry estimating eventual download speeds up to 1,000 times higher than 4G, potentially exceeding 10Gbps. This may not happen immediately, but such speeds are in fact faster than most – if not all – current UK consumer fixed broadband lines, fibre and cable included. This could be another early indication that 5G will transform the telecommunication industry as a whole, and not just mobile.

Latency - a measurement of how long it takes the network to respond to a request - is another key consideration, especially given the types of applications we’re anticipating for 5G. The expected reductions here will make it ideal for use within new industries or applications where instantaneous response is required, such as autonomous vehicles and virtual reality.

Where can 5G really add value?

The most obvious application is improved mobile broadband, with 5G building on the already-impressive capabilities of its predecessor. This means easier, faster and higher quality communication for businesses and consumers.

The next is Fixed Wireless Access (FWA), as in many instances 5G will offer better speeds and lower costs than fixed broadband solutions, such as fibre and cable; this will be the case in urban and rural areas under certain conditions.

This new generation of connectivity will have a huge impact on ‘smart’ machine-to-machine technologies, with the improvements in speed, latency, network resilience and electricity consumption in particular allowing all manner of devices and sensors to communicate effectively and in real-time. 5G is also likely to allow a much lower cost-per-application for the Internet of Things (IoT), opening up the possibility of mass roll outs of new applications where current costs would be too high.

This includes connected – and even driverless - cars, for which 4G networks aren’t quite sufficient. Our surroundings will become smarter too; expect largely automated cities and more efficient homes as the ‘things’ within them become better at interacting with one another.

The IoT will evolve from concept into reality, enabling vital connections between people and processes. Future applications could include real-time healthcare monitoring, smart agriculture and sensor-driven traffic management.

It’s impossible to know exactly where 5G will be applied, but it’s safe to say it will help in allowing the UK to become truly digital.



Challenges to consider

5G networks will be very different from today’s 3G and 4G networks. They require real-time, end-to-end visibility; automated, intelligent resource analysis and allocation; and most importantly, they will be underpinned by software-defined networking (SDN) control.

The success of 5G, therefore, and whether its potential is fulfilled, will depend on how these components are deployed and managed – and that’s something Arqiva is heavily involved with. We are a key enabler of this new generation of wireless communication.

There are many challenges to consider, though. The first and biggest is spectrum.  

5G will be extremely spectrum-hungry, so the greatest amount of spectrum must be made available to the mobile industry as a priority - and available at a price that still allows companies to invest and innovate. What’s more, this spectrum will need to be positioned in high-frequency bands to allow for high speeds and low latency coverage.

The GSMA has said that the 28GHz band is of particular interest for 5G – its use has already been permitted in the United States and is being closely studied in South Korea and Japan. If used, it would complement the 26GHz band already supported by the European Union, as the same equipment could easily support both bands. This would help to lower device costs and speed up global adoption. The 28GHz band is also the only one that allows full 5G speeds, as it isn’t as fragmented and congested as the 26GHz band.

Then, re-farming the current 2G, 3G and 4G spectrum bands into 5G will be crucial, and the sooner Ofcom provides visibility on this key topic, the better, as each MNO needs time to define its 5G network strategy.

The Government must also revisit business rates – the densification of networks from dozens of thousands of towers to hundreds of thousands of small cells will increase these costs to unsustainable levels, jeopardising innovation and investment.

There is also the challenge of avoiding the creation of a digital divide between urban areas and rural areas, as materially densifying rural networks or rolling out high-frequency spectrum bands in these environments would be economically challenging. The Government could consider modifying the spectrum auction rules to lower prices and annual fees against 5G coverage obligations, especially along railways and main roads.

5G can only be rolled out at scale if dark fibre is available with the right capability to reach each mobile tower and each small cell, and at competitive prices – to deliver both 5G speed and low latency. Such capability and affordability are not yet available in the UK, so this is a key challenge to solve in the years to come.

The mass deployment of hundreds of thousands of small cells, as well as a material densification of rural towers, will require the rules and regulations around telecommunication network roll-outs to be relaxed. Significant progress has already been made here thanks to the Government, but further changes will need to be considered.

Many would see the points above as an insurmountable list of obstacles. At Arqiva, we see them as challenging opportunities - the kinds of challenges we will help to resolve.

A brighter, connected future

People often talk about 5G as though it’s something from the distant future, but we’re really not far from the first networks arriving. Most of those involved in its roll-out have cited 2020 as the target, but if trials continue to go well and the various regulatory challenges are solved, the process could be even quicker than anticipated.

We, along with the rest of the industry, are already working hard to prepare. This way, we can ensure we get everything right first time and be ready when the time does come.

With all of the above in mind, it seems clear that 5G – the next, and arguably biggest, era of communication – for mobile and fixed – will help to create an incredibly vibrant digital economy for the UK.

Need of 5G Wireless System

If we will compare 5G with other generation mobile phones, it is obvious that 5G has some extraordinary features and advantages.

1). 5G has better coverage area and high data rate at the edge of the cell.

2). It has low battery consumption.

3). Availability of multiple data transfer path.

4). Around 1 Gbps data rate is easily possible.

5). Security is more.

6). Energy efficiency and spectral efficiency are good.

Due to the above advantages, 5th generation wireless system is very much essential.
