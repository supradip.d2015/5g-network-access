package in.android5g.fiveg;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    /* access modifiers changed from: private */
    public ListViewAdapter adapter;
    /* access modifiers changed from: private */
    public ListView listView;
    /* access modifiers changed from: private */
    public String selected;
    private ArrayList<String> stringArrayList;

    private void setData() {
        this.stringArrayList = new ArrayList<>();
        this.stringArrayList.add("5G - Overview");
        this.stringArrayList.add("Salient Features of 5G");
        this.stringArrayList.add("5G - Technology");
        this.stringArrayList.add("What is 5G Technology?");
        this.stringArrayList.add("5G - Architecture");
        this.stringArrayList.add("Architecture of 5G");
        this.stringArrayList.add("The Master Core Technology");
        this.stringArrayList.add("5G - Time Period Required");
        this.stringArrayList.add("5G - Applications");
        this.stringArrayList.add("5G - Advancement");
        this.stringArrayList.add("5G - Advantages & Disadvantages");
        this.stringArrayList.add("Disadvantages of 5G Technology");
        this.stringArrayList.add("5G - Challenges");
        this.stringArrayList.add("Technological Challenges");
        this.stringArrayList.add("Common Challenges");
        this.stringArrayList.add("5G Super Core Concept");
        this.stringArrayList.add("5G - Future Scope");
        this.stringArrayList.add("Future Span of 5G");
    }

    public void onBackPressed() {
        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawerLayout.isDrawerOpen((int) GravityCompat.START)) {
            drawerLayout.closeDrawer((int) GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_main);
        this.listView = (ListView) findViewById(R.id.list_item);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final SharedPreferences.Editor edit = defaultSharedPreferences.edit();
        if (defaultSharedPreferences.getBoolean("IS_FIRST_RUN", true)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle((CharSequence) "Policy Notice");
            builder.setMessage((CharSequence) "By Using This Application, You Agree to Our Privacy Policy.");
            builder.setIcon((int) R.drawable.ic_check_circle_black_24dp);
            builder.setCancelable(false);
            builder.setPositiveButton((CharSequence) "AGREE", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    edit.putBoolean("IS_FIRST_RUN", false);
                    edit.commit();
                    dialogInterface.dismiss();
                }
            });
            builder.setNegativeButton((CharSequence) "DENY", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    PreferenceManager.getDefaultSharedPreferences(MainActivity.this.getApplicationContext());
                    SharedPreferences.Editor edit = defaultSharedPreferences.edit();
                    edit.putBoolean("IS_FIRST_RUN", true);
                    edit.commit();
                    dialogInterface.dismiss();
                    MainActivity.this.onBackPressed();
                }
            });
            builder.setNeutralButton((CharSequence) "PRIVACY POLICY", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent(MainActivity.this, InformationActivity.class);
                    intent.putExtra(FirebaseAnalytics.Param.LEVEL, "http://intelitech.in/privacy_policy.htm");
                    MainActivity.this.startActivity(intent);
                }
            });
            builder.create().show();
        }
        MobileAds.initialize(getApplicationContext(), "ca-app-pub-4297693571865380~8105248554");
        ((AdView) findViewById(R.id.adView_main)).loadAd(new AdRequest.Builder().build());
        setData();
        this.adapter = new ListViewAdapter(this, R.layout.item_listview, this.stringArrayList);
        this.listView.setAdapter(this.adapter);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                String unused = MainActivity.this.selected = MainActivity.this.listView.getItemAtPosition(i).toString();
                if (MainActivity.this.selected.equals("5G - Overview")) {
                    Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                    intent.putExtra("id", i);
                    intent.putExtra(FirebaseAnalytics.Param.LEVEL, "1");
                    intent.putExtra(FirebaseAnalytics.Param.VALUE, MainActivity.this.adapter.getItem(i));
                    MainActivity.this.startActivity(intent);
                }
                if (MainActivity.this.selected.equals("Salient Features of 5G")) {
                    Intent intent2 = new Intent(MainActivity.this, DetailActivity.class);
                    intent2.putExtra("id", i);
                    intent2.putExtra(FirebaseAnalytics.Param.LEVEL, "2");
                    intent2.putExtra(FirebaseAnalytics.Param.VALUE, MainActivity.this.adapter.getItem(i));
                    MainActivity.this.startActivity(intent2);
                }
                if (MainActivity.this.selected.equals("5G - Technology")) {
                    Intent intent3 = new Intent(MainActivity.this, DetailActivity.class);
                    intent3.putExtra("id", i);
                    intent3.putExtra(FirebaseAnalytics.Param.LEVEL, "3");
                    intent3.putExtra(FirebaseAnalytics.Param.VALUE, MainActivity.this.adapter.getItem(i));
                    MainActivity.this.startActivity(intent3);
                }
                if (MainActivity.this.selected.equals("What is 5G Technology?")) {
                    Intent intent4 = new Intent(MainActivity.this, DetailActivity.class);
                    intent4.putExtra("id", i);
                    intent4.putExtra(FirebaseAnalytics.Param.LEVEL, "4");
                    intent4.putExtra(FirebaseAnalytics.Param.VALUE, MainActivity.this.adapter.getItem(i));
                    MainActivity.this.startActivity(intent4);
                }
                if (MainActivity.this.selected.equals("5G - Architecture")) {
                    Intent intent5 = new Intent(MainActivity.this, DetailActivity.class);
                    intent5.putExtra("id", i);
                    intent5.putExtra(FirebaseAnalytics.Param.LEVEL, "5");
                    intent5.putExtra(FirebaseAnalytics.Param.VALUE, MainActivity.this.adapter.getItem(i));
                    MainActivity.this.startActivity(intent5);
                }
                if (MainActivity.this.selected.equals("Architecture of 5G")) {
                    Intent intent6 = new Intent(MainActivity.this, DetailActivity.class);
                    intent6.putExtra("id", i);
                    intent6.putExtra(FirebaseAnalytics.Param.LEVEL, "6");
                    intent6.putExtra(FirebaseAnalytics.Param.VALUE, MainActivity.this.adapter.getItem(i));
                    MainActivity.this.startActivity(intent6);
                }
                if (MainActivity.this.selected.equals("The Master Core Technology")) {
                    Intent intent7 = new Intent(MainActivity.this, DetailActivity.class);
                    intent7.putExtra("id", i);
                    intent7.putExtra(FirebaseAnalytics.Param.LEVEL, "7");
                    intent7.putExtra(FirebaseAnalytics.Param.VALUE, MainActivity.this.adapter.getItem(i));
                    MainActivity.this.startActivity(intent7);
                }
                if (MainActivity.this.selected.equals("5G - Time Period Required")) {
                    Intent intent8 = new Intent(MainActivity.this, DetailActivity.class);
                    intent8.putExtra("id", i);
                    intent8.putExtra(FirebaseAnalytics.Param.LEVEL, "8");
                    intent8.putExtra(FirebaseAnalytics.Param.VALUE, MainActivity.this.adapter.getItem(i));
                    MainActivity.this.startActivity(intent8);
                }
                if (MainActivity.this.selected.equals("5G - Applications")) {
                    Intent intent9 = new Intent(MainActivity.this, DetailActivity.class);
                    intent9.putExtra("id", i);
                    intent9.putExtra(FirebaseAnalytics.Param.LEVEL, "9");
                    intent9.putExtra(FirebaseAnalytics.Param.VALUE, MainActivity.this.adapter.getItem(i));
                    MainActivity.this.startActivity(intent9);
                }
                if (MainActivity.this.selected.equals("5G - Advancement")) {
                    Intent intent10 = new Intent(MainActivity.this, DetailActivity.class);
                    intent10.putExtra("id", i);
                    intent10.putExtra(FirebaseAnalytics.Param.LEVEL, "10");
                    intent10.putExtra(FirebaseAnalytics.Param.VALUE, MainActivity.this.adapter.getItem(i));
                    MainActivity.this.startActivity(intent10);
                }
                if (MainActivity.this.selected.equals("5G - Advantages & Disadvantages")) {
                    Intent intent11 = new Intent(MainActivity.this, DetailActivity.class);
                    intent11.putExtra("id", i);
                    intent11.putExtra(FirebaseAnalytics.Param.LEVEL, "11");
                    intent11.putExtra(FirebaseAnalytics.Param.VALUE, MainActivity.this.adapter.getItem(i));
                    MainActivity.this.startActivity(intent11);
                }
                if (MainActivity.this.selected.equals("Disadvantages of 5G Technology")) {
                    Intent intent12 = new Intent(MainActivity.this, DetailActivity.class);
                    intent12.putExtra("id", i);
                    intent12.putExtra(FirebaseAnalytics.Param.LEVEL, "12");
                    intent12.putExtra(FirebaseAnalytics.Param.VALUE, MainActivity.this.adapter.getItem(i));
                    MainActivity.this.startActivity(intent12);
                }
                if (MainActivity.this.selected.equals("5G - Challenges")) {
                    Intent intent13 = new Intent(MainActivity.this, DetailActivity.class);
                    intent13.putExtra("id", i);
                    intent13.putExtra(FirebaseAnalytics.Param.LEVEL, "13");
                    intent13.putExtra(FirebaseAnalytics.Param.VALUE, MainActivity.this.adapter.getItem(i));
                    MainActivity.this.startActivity(intent13);
                }
                if (MainActivity.this.selected.equals("Technological Challenges")) {
                    Intent intent14 = new Intent(MainActivity.this, DetailActivity.class);
                    intent14.putExtra("id", i);
                    intent14.putExtra(FirebaseAnalytics.Param.LEVEL, "14");
                    intent14.putExtra(FirebaseAnalytics.Param.VALUE, MainActivity.this.adapter.getItem(i));
                    MainActivity.this.startActivity(intent14);
                }
                if (MainActivity.this.selected.equals("Common Challenges")) {
                    Intent intent15 = new Intent(MainActivity.this, DetailActivity.class);
                    intent15.putExtra("id", i);
                    intent15.putExtra(FirebaseAnalytics.Param.LEVEL, "15");
                    intent15.putExtra(FirebaseAnalytics.Param.VALUE, MainActivity.this.adapter.getItem(i));
                    MainActivity.this.startActivity(intent15);
                }
                if (MainActivity.this.selected.equals("5G Super Core Concept")) {
                    Intent intent16 = new Intent(MainActivity.this, DetailActivity.class);
                    intent16.putExtra("id", i);
                    intent16.putExtra(FirebaseAnalytics.Param.LEVEL, "16");
                    intent16.putExtra(FirebaseAnalytics.Param.VALUE, MainActivity.this.adapter.getItem(i));
                    MainActivity.this.startActivity(intent16);
                }
                if (MainActivity.this.selected.equals("5G - Future Scope")) {
                    Intent intent17 = new Intent(MainActivity.this, DetailActivity.class);
                    intent17.putExtra("id", i);
                    intent17.putExtra(FirebaseAnalytics.Param.LEVEL, "17");
                    intent17.putExtra(FirebaseAnalytics.Param.VALUE, MainActivity.this.adapter.getItem(i));
                    MainActivity.this.startActivity(intent17);
                }
                if (MainActivity.this.selected.equals("Future Span of 5G")) {
                    Intent intent18 = new Intent(MainActivity.this, DetailActivity.class);
                    intent18.putExtra("id", i);
                    intent18.putExtra(FirebaseAnalytics.Param.LEVEL, "18");
                    intent18.putExtra(FirebaseAnalytics.Param.VALUE, MainActivity.this.adapter.getItem(i));
                    MainActivity.this.startActivity(intent18);
                }
            }
        });
        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        ((NavigationView) findViewById(R.id.nav_view)).setNavigationItemSelectedListener(this);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        ((SearchView) menu.findItem(R.id.action_search).getActionView()).setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String str) {
                if (TextUtils.isEmpty(str)) {
                    MainActivity.this.adapter.filter("");
                    MainActivity.this.listView.clearTextFilter();
                    return true;
                }
                MainActivity.this.adapter.filter(str);
                return true;
            }

            public boolean onQueryTextSubmit(String str) {
                return false;
            }
        });
        return true;
    }

    public boolean onNavigationItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == R.id.nav_camera) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.android.in")));
        } else if (itemId == R.id.nav_gallery) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://www.facebook.com/androidInc/")));
        } else if (itemId == R.id.nav_slideshow) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/developer?id=android")));
        } else if (itemId == R.id.nav_share) {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.SEND");
            intent.putExtra("android.intent.extra.TEXT", " Hey,Get the 5G Technology App on Google Play, To download https://goo.gl/9ml6hW ");
            intent.setType("text/plain");
            startActivity(Intent.createChooser(intent, "Share with"));
        } else if (itemId == R.id.nav_send) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=in.android.fiveg")));
        } else if (itemId == R.id.nav_feedback) {
            Intent intent2 = new Intent("android.intent.action.SENDTO");
            intent2.setData(Uri.parse("mailto: contact@android.in"));
            intent2.putExtra("android.intent.extra.SUBJECT", "Feedback for 5G Technology (1.0)");
            startActivity(Intent.createChooser(intent2, "Send feedback"));
        } else if (itemId == R.id.terms_of_use) {
            Intent intent3 = new Intent(this, InformationActivity.class);
            intent3.putExtra(FirebaseAnalytics.Param.LEVEL, "http://android.in/tac.htm");
            startActivity(intent3);
        } else if (itemId == R.id.faq) {
            Intent intent4 = new Intent(this, InformationActivity.class);
            intent4.putExtra(FirebaseAnalytics.Param.LEVEL, "http://android.in/app_faq.html");
            startActivity(intent4);
        } else if (itemId == R.id.privacy_policy) {
            Intent intent5 = new Intent(this, InformationActivity.class);
            intent5.putExtra(FirebaseAnalytics.Param.LEVEL, "http://android.in/privacy_policy.htm");
            startActivity(intent5);
        }
        ((DrawerLayout) findViewById(R.id.drawer_layout)).closeDrawer((int) GravityCompat.START);
        return true;
    }
}

