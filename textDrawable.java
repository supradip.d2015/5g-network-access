package com.android5g.textdrawable;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.RectShape;
import android.graphics.drawable.shapes.RoundRectShape;

public class TextDrawable extends ShapeDrawable {
    private static final float SHADE_FACTOR = 0.9f;
    private final Paint borderPaint;
    private final int borderThickness;
    private final int color;
    private final int fontSize;
    private final int height;
    private final float radius;
    private final RectShape shape;
    private final String text;
    private final Paint textPaint;
    private final int width;

    public static class Builder implements IConfigBuilder, IShapeBuilder, IBuilder {
        /* access modifiers changed from: private */
        public int borderThickness;
        /* access modifiers changed from: private */
        public int color;
        /* access modifiers changed from: private */
        public Typeface font;
        /* access modifiers changed from: private */
        public int fontSize;
        /* access modifiers changed from: private */
        public int height;
        /* access modifiers changed from: private */
        public boolean isBold;
        public float radius;
        /* access modifiers changed from: private */
        public RectShape shape;
        /* access modifiers changed from: private */
        public String text;
        public int textColor;
        /* access modifiers changed from: private */
        public boolean toUpperCase;
        /* access modifiers changed from: private */
        public int width;

        private Builder() {
            this.text = "";
            this.color = -7829368;
            this.textColor = -1;
            this.borderThickness = 0;
            this.width = -1;
            this.height = -1;
            this.shape = new RectShape();
            this.font = Typeface.create("sans-serif-light", 0);
            this.fontSize = -1;
            this.isBold = false;
            this.toUpperCase = false;
        }

        public IConfigBuilder beginConfig() {
            return this;
        }

        public IConfigBuilder bold() {
            this.isBold = true;
            return this;
        }

        public TextDrawable build(String str, int i) {
            this.color = i;
            this.text = str;
            return new TextDrawable(this);
        }

        public TextDrawable buildRect(String str, int i) {
            rect();
            return build(str, i);
        }

        public TextDrawable buildRound(String str, int i) {
            round();
            return build(str, i);
        }

        public TextDrawable buildRoundRect(String str, int i, int i2) {
            roundRect(i2);
            return build(str, i);
        }

        public IShapeBuilder endConfig() {
            return this;
        }

        public IConfigBuilder fontSize(int i) {
            this.fontSize = i;
            return this;
        }

        public IConfigBuilder height(int i) {
            this.height = i;
            return this;
        }

        public IBuilder rect() {
            this.shape = new RectShape();
            return this;
        }

        public IBuilder round() {
            this.shape = new OvalShape();
            return this;
        }

        public IBuilder roundRect(int i) {
            float f = (float) i;
            this.radius = f;
            this.shape = new RoundRectShape(new float[]{f, f, f, f, f, f, f, f}, (RectF) null, (float[]) null);
            return this;
        }

        public IConfigBuilder textColor(int i) {
            this.textColor = i;
            return this;
        }

        public IConfigBuilder toUpperCase() {
            this.toUpperCase = true;
            return this;
        }

        public IConfigBuilder useFont(Typeface typeface) {
            this.font = typeface;
            return this;
        }

        public IConfigBuilder width(int i) {
            this.width = i;
            return this;
        }

        public IConfigBuilder withBorder(int i) {
            this.borderThickness = i;
            return this;
        }
    }

    public interface IBuilder {
        TextDrawable build(String str, int i);
    }

    public interface IConfigBuilder {
        IConfigBuilder bold();

        IShapeBuilder endConfig();

        IConfigBuilder fontSize(int i);

        IConfigBuilder height(int i);

        IConfigBuilder textColor(int i);

        IConfigBuilder toUpperCase();

        IConfigBuilder useFont(Typeface typeface);

        IConfigBuilder width(int i);

        IConfigBuilder withBorder(int i);
    }

    public interface IShapeBuilder {
        IConfigBuilder beginConfig();

        TextDrawable buildRect(String str, int i);

        TextDrawable buildRound(String str, int i);

        TextDrawable buildRoundRect(String str, int i, int i2);

        IBuilder rect();

        IBuilder round();

        IBuilder roundRect(int i);
    }

    private TextDrawable(Builder builder) {
        super(builder.shape);
        this.shape = builder.shape;
        this.height = builder.height;
        this.width = builder.width;
        this.radius = builder.radius;
        this.text = builder.toUpperCase ? builder.text.toUpperCase() : builder.text;
        this.color = builder.color;
        this.fontSize = builder.fontSize;
        this.textPaint = new Paint();
        this.textPaint.setColor(builder.textColor);
        this.textPaint.setAntiAlias(true);
        this.textPaint.setFakeBoldText(builder.isBold);
        this.textPaint.setStyle(Paint.Style.FILL);
        this.textPaint.setTypeface(builder.font);
        this.textPaint.setTextAlign(Paint.Align.CENTER);
        this.textPaint.setStrokeWidth((float) builder.borderThickness);
        this.borderThickness = builder.borderThickness;
        this.borderPaint = new Paint();
        this.borderPaint.setColor(getDarkerShade(this.color));
        this.borderPaint.setStyle(Paint.Style.STROKE);
        this.borderPaint.setStrokeWidth((float) this.borderThickness);
        getPaint().setColor(this.color);
    }

    public static IShapeBuilder builder() {
        return new Builder();
    }

    private void drawBorder(Canvas canvas) {
        RectF rectF = new RectF(getBounds());
        rectF.inset((float) (this.borderThickness / 2), (float) (this.borderThickness / 2));
        if (this.shape instanceof OvalShape) {
            canvas.drawOval(rectF, this.borderPaint);
        } else if (this.shape instanceof RoundRectShape) {
            canvas.drawRoundRect(rectF, this.radius, this.radius, this.borderPaint);
        } else {
            canvas.drawRect(rectF, this.borderPaint);
        }
    }

    private int getDarkerShade(int i) {
        return Color.rgb((int) (((float) Color.red(i)) * SHADE_FACTOR), (int) (((float) Color.green(i)) * SHADE_FACTOR), (int) (((float) Color.blue(i)) * SHADE_FACTOR));
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        Rect bounds = getBounds();
        if (this.borderThickness > 0) {
            drawBorder(canvas);
        }
        int save = canvas.save();
        canvas.translate((float) bounds.left, (float) bounds.top);
        int width2 = this.width < 0 ? bounds.width() : this.width;
        int height2 = this.height < 0 ? bounds.height() : this.height;
        this.textPaint.setTextSize((float) (this.fontSize < 0 ? Math.min(width2, height2) / 2 : this.fontSize));
        canvas.drawText(this.text, (float) (width2 / 2), ((float) (height2 / 2)) - ((this.textPaint.descent() + this.textPaint.ascent()) / 2.0f), this.textPaint);
        canvas.restoreToCount(save);
    }

    public int getIntrinsicHeight() {
        return this.height;
    }

    public int getIntrinsicWidth() {
        return this.width;
    }

    public int getOpacity() {
        return -3;
    }

    public void setAlpha(int i) {
        this.textPaint.setAlpha(i);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.textPaint.setColorFilter(colorFilter);
    }
}
